<?php
// $Id$

/**
 * @file
 * Views 2 hooks and callback registries.
 */

/**
 * Implementation of hook_views_data().
 */
function uc_order_uploads_views_data() {
  $data = array();

  $data['uc_order_product_uploads']['table']['group'] = t('Ubercart order uploads');

  $data['uc_order_product_uploads']['table']['join']['uc_orders'] = array(
    'left_table' => 'uc_order_products',
    'left_field' => 'order_product_id',
    'field' => 'order_product_id',
  );

  $data['uc_order_product_uploads']['order_product_upload_id'] = array(
    'title' => t('Order Upload Id'),
    'help' => t('The Upload Id corresponding to Order Product.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['uc_order_product_uploads']['order_product_id'] = array(
    'title' => t('Order Product Id'),
    'help' => t('The Order Product ID within an Order. Not NId of Product Node.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['uc_order_product_uploads']['filename'] = array(
    'title' => t('Uploaded File Name'),
    'help' => t('File Name for Order Upload.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['uc_order_product_uploads']['filemime'] = array(
    'title' => t('Upload File Mime Type'),
    'help' => t('File type depicting Mime Type for Uploaded file.'),
    'field' => array(
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['uc_order_product_uploads']['filesize'] = array(
    'title' => t('Upload File Size'),
    'help' => t('Uploaded file size in bytes.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['uc_order_product_uploads']['status'] = array(
    'title' => t('Upload Status'),
    'help' => t('Uploaded File status such as Via Checkout, Via Store, etc.'),
    'field' => array(
      'handler' => 'uc_order_uploads_handler_field_upload_status',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['uc_order_product_uploads']['timestamp'] = array(
    'title' => t('Upload Timestamp'),
    'help' => t('Timestamp value for File Upload.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_date',
    ),
  );

  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function uc_order_uploads_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'uc_order_uploads') .'/views',
    ),
    'handlers' => array(
      'uc_order_uploads_handler_field_upload_status' => array(
        'parent' => 'views_handler_field_numeric',
      ),
    ),
  );
}
