<?php
// $Id$

/**
 * @file
 * Views handler: Uploaded File Status.
 */

/**
 * Return String Uploaded File Status value to display in the View.
 */
class uc_order_uploads_handler_field_upload_status extends views_handler_field_numeric {
  function construct() {
    parent::construct();
  }

  function render($values) {
    $upload_status = array(
      '1' => t('(Via Checkout)'),
      '2' => t('(Via Store)'),
      '3' => t('(Via Customer)'),
      '6' => t('(Via Checkout, Frozen by Customer)'),
      '7' => t('(Via Store, Frozen by Customer)'),
      '8' => t('(Via Customer, Frozen by Customer)'),
      '11' => t('(Via Checkout, Frozen by Store)'),
      '12' => t('(Via Store, Frozen by Store)'),
      '13' => t('(Via Customer, Frozen by Store)'),
    );

    return $upload_status[$values->uc_order_product_uploads_status];
  }
}
