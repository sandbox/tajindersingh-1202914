<?php
// $Id$

/**
 * @file
 * Product administration menu items.
 */

function uc_order_uploads_settings_form() {
  $form = array();

  $form['uc_order_uploads_upload_title'] = array(
    '#title' => t('Order Upload title'),
    '#type' => 'textfield',
    '#description' => t('Title to be shown for uploaded files. By default it is Upload i.e. Upload 1, Upload 2, etc.'),
    '#required' => TRUE,
    '#default_value' => t(variable_get('uc_order_uploads_upload_title', 'Upload')),
  );

  $form['uc_order_uploads_node_level'] = array(
    '#title' => t('Individual Node settings'),
    '#type' => 'checkbox',
    '#description' => t('Node level Order Uploads setting on node form.'),
    '#default_value' => variable_get('uc_order_uploads_node_level', FALSE),
  );

  $form['uc_order_uploads_store_level'] = array(
    '#title' => t('Store Order Uploads settings'),
    '#type' => 'fieldset',
    '#description' => t('Store level Order Uploads when Node level settings disabled.'),
  );

  $form['uc_order_uploads_store_level']['uc_order_uploads_optional'] = array(
    '#type' => 'checkbox',
    '#title' => t('Optional'),
    '#description' => t('It is optional to upload file on checkout as per Customers wish.'),
    '#default_value' => variable_get('uc_order_uploads_optional', FALSE),
  );

  $form['uc_order_uploads_store_level']['uc_order_uploads_allowedfiles'] = array(
    '#type' => 'textfield',
    '#title' => t('Allowed Files'),
    '#description' => t('Space OR Comma separated list of extensions to specified allowed file types. Spaces will be converted to Commas.'),
    '#default_value' => variable_get('uc_order_uploads_allowedfiles', ''),
    '#required' => TRUE,
  );

  $form['uc_order_uploads_store_level']['uc_order_uploads_allowedsize'] = array(
    '#type' => 'textfield',
    '#title' => t('Allowed Max Size'),
    '#description' => t('Specify maximum file upload size as 100 for Bytes, 50K for Kilobytes, 5M for Megabytes, and so on'),
    '#default_value' => variable_get('uc_order_uploads_allowedsize', ''),
    '#required' => TRUE,
  );

  $form['uc_order_uploads_product_classes'] = array(
    '#title' => t('Enable Order Uploads for product classes'),
    '#type' => 'fieldset',
    '#description' => t('Choose for which Product classes you want to enable Order Uploads'),
  );

  $product_types = uc_product_types();
  foreach ($product_types as $product_type) {
    $form['uc_order_uploads_product_classes']['uc_order_uploads_for_' . $product_type] = array(
      '#title' => ucwords($product_type),
      '#type' => 'checkbox',
      '#description' => t('Enable Order Uploads for class ') . $product_type,
      '#default_value' => variable_get('uc_order_uploads_for_' . $product_type, FALSE),
    );
  }

  $form['#validate'][] = 'uc_order_uploads_settings_form_validate';

  return system_settings_form($form);
}

/**
 * see uc_order_uploads_settings_form
 * @form form object
 * @form_state form submitted data
 */
function uc_order_uploads_settings_form_validate($form, &$form_state) {
  $form_state['values']['uc_order_uploads_allowedfiles'] = trim($form_state['values']['uc_order_uploads_allowedfiles']);

  if (empty($form_state['values']['uc_order_uploads_allowedfiles'])) {
    form_set_error('uc_order_uploads_allowedfiles', t('Allowed file extensions missing.'));
  }
  else {
    $to_null = trim(str_replace(array(' ', ','), '', $form_state['values']['uc_order_uploads_allowedfiles']));

    if (drupal_strlen($to_null) == 0) {
      form_set_error('uc_order_uploads_allowedfiles', t('Invalid allowed file extensions.'));
    }
    else {
      $exts = explode(' ', $form_state['values']['uc_order_uploads_allowedfiles']);
      foreach ($exts as $key => $ext) {
        $exts[$key] = trim($ext, ' ,');
      }

      $exts = implode(',', $exts);

      while (!strpos($exts, ',,') === FALSE) {
        $exts = str_replace(',,', ',', $exts);
      }

      $form_state['values']['uc_order_uploads_allowedfiles'] = drupal_strtoupper($exts);
    }
  }

  $form_state['values']['uc_order_uploads_allowedsize'] = trim($form_state['values']['uc_order_uploads_allowedsize']);
  if (preg_match('/[^A-Za-z0-9]/', $form_state['values']['uc_order_uploads_allowedsize']) !== 0) {
    form_set_error('uc_order_uploads_allowedsize', t('Invalid allowed maximum file upload size.'));
  }
  elseif (empty($form_state['values']['uc_order_uploads_allowedsize'])) {
    form_set_error('uc_order_uploads_allowedsize', t('Allowed maximum file upload size missing.'));
  }
  elseif (is_numeric($form_state['values']['uc_order_uploads_allowedsize']) == FALSE) {
    $size_units = array('K', 'M', 'G', 'T', 'P', 'E');
    $size_unit = drupal_strtoupper(drupal_substr($form_state['values']['uc_order_uploads_allowedsize'], -1));
    if (!in_array($size_unit, $size_units)) {
      form_set_error('uc_order_uploads_allowedsize', t('Invalid allowed maximum file upload size.'));
    }
    else {
      $form_state['values']['uc_order_uploads_allowedsize'] = trim($form_state['values']['uc_order_uploads_allowedsize']);
      $form_state['values']['uc_order_uploads_allowedsize'] = drupal_strtoupper($form_state['values']['uc_order_uploads_allowedsize']);
    }
  }
}
